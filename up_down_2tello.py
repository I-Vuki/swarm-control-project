import socket
import time

sock1 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock1.setsockopt(socket.SOL_SOCKET, 25, 'wlp2s0'.encode())

sock2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock2.setsockopt(socket.SOL_SOCKET, 25, 'wlx503eaa6d8395'.encode())


sock1.sendto('command'.encode(), 0, ('192.168.10.1', 8889))
res = sock1.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("1 command Message receive: " + res.decode() + " from Tello")



sock2.sendto('command'.encode(), 0, ('192.168.10.1', 8889))
res = sock2.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("2 command Message receive: " + res.decode() + " from Tello")



sock1.sendto('takeoff'.encode(), 0, ('192.168.10.1', 8889))
res = sock1.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("1 takeoff Message receive: " + res.decode() + " from Tello")

sock2.sendto('takeoff'.encode(), 0, ('192.168.10.1', 8889))
res = sock2.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("2 takeoff Message receive: " + res.decode() + " from Tello")



sock1.sendto('forward 100'.encode(), 0, ('192.168.10.1', 8889))
res = sock1.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("1 fw Message receive: " + res.decode() + " from Tello")

sock2.sendto('forward 100'.encode(), 0, ('192.168.10.1', 8889))
res = sock2.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("2 fw Message receive: " + res.decode() + " from Tello")



sock1.sendto('land'.encode(), 0, ('192.168.10.1', 8889))
res = sock1.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("1 lnd Message receive: " + res.decode() + " from Tello")

sock2.sendto('land'.encode(), 0, ('192.168.10.1', 8889))
res = sock2.recv(128) #ovdije ide recvform ako nas zanima i addr arg je velicina
print("2 lnd Message receive: " + res.decode() + " from Tello")
