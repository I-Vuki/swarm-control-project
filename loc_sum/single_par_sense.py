n = 5
P = [1./n] * n
W = ['g','r','r','g','g']
Z = 'g'
pH = 0.6
pM = 0.2
def sense(p,Z):
    q = [0] * len(p)
    for i in range(len(p)):
        if W[i] == Z:
            q[i] = p[i] * pH
        else:
            q[i] = p[i] * pM
    S = sum(q)
    for i in range(len(q)):
        q[i] /= S
    return q

print sense(P,Z)
