def sense(colors, measurement, p, sensor_right):
    pHit = sensor_right
    pMiss = 1-sensor_right
    lr = len(colors)
    ls = len(colors[0])
    S = 0
    for i in range(lr):
        for j in range(ls):
            if colors[i][j] == measurement:
                p[i][j] *= pHit
            else:
                p[i][j] *= pMiss

    for i in range(lr):
        S += sum(p[i])
    for i in range(lr):
        for j in range(ls):
            p[i][j] /= S

    return p
def move(p,motion,p_move):
    lr = len(p)
    ls =  len(p[0])
    q = [0] * lr
    for i in range(lr):
        q[i] = [0] * ls
        for j in range(ls):
            q[i][j] += p[(i-motion[0])%lr][(j-motion[1])%ls] * p_move 
            q[i][j] += p[i][j] * (1-p_move)
    return q
    
def localize(colors, measurements, motions, sensor_right, p_move):
    lr = len(colors)
    ls = len(colors[0])
    n = lr * ls
    p = [0] * lr
    for i in range(lr):
        p[i] = [1./n] * ls
    for i in range(len(measurements)):
        p = move(p,motions[i], p_move)
        p = sense(colors, measurements[i], p, sensor_right)
    return p

colors = [['G', 'G', 'G'],
          ['G', 'R', 'R'],
          ['G', 'G', 'G']]
measurements = ['R','R']
motions = [[0,0],[0,1]]
sensor_right = 1.0
p_move = 0.5

p = localize(colors,measurements, motions, sensor_right, p_move)
for i in range(len(p)):
    print p[i]
