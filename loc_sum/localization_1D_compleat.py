'''
osnone vrijednosti
'''
W = ['g','r','b','r','b','r','g'] #svijet sa markerima g i r su dvije vrste markera
n = len(W) # duljina naseg svijet tj. kokiko ima celija
P = [1./n] * n # polje vjerojatnosti da smo u pojedinoj celiji inicijalzirano sve na istu vrijednost, suma mora biti 1

'''
vectori koji nam trebaju za mjerenja odnosno pomicanje
'''
measure = ['b','r','g'] #mjerena koja robot vidi svojim senzorima
motions = [1, 1, 1] #pomaci koje robot radi, u nasem slujacu uvijek u desno

'''
koeficijenti za racunanje kod sense i move funkcija
'''
pHit = 0.6 #koeficijent kojim mnozimo celije u kojima vjerojatnije jesno
pMiss = 0.2 #koeficijent kojim mnozimo celije u kojima vjerojatno nismo
pExact = 0.8 #koeficijent da smo dosli di smo htijeli pomakom
pOver = 0.1 # koef da smo otisli predaleko
pUnder = 0.1 # koef da smo prerano stali


'''
f-ja sense prima polje vjerojatnosti p
i neko znanje Z (informaciju/mjerenje)
te vrati polje novih vrijednosti uz poznat Z
'''
def sense(p,Z):
    l = len(p) # pomocna varijabla kako bi se duljina samo jednomracunala
    q = [0] * l # inicijalzajcija pomocnog polja iste duzine kao p
    for i in range(l): # mnozimo sa koeficijentima za vjerojatnost da smo u nekoj celiji bazirano na mjerenjima
        if W[i] == Z:
            q[i] = p[i] * pHit
        else:
            q[i] = p[i] * pMiss

    S = sum(q)
    for i in range(len(q)): #normalizacija dijelimo svaki element sa trenutjnom sumom kako bi dobili sumu koja je 1 (u vjerojatnosti ukupna suma uvijek mora biti 1)
        q[i] /= S

    return q

'''
funkcija prima polje vjeroajtnosti p
i broj koraka U za koji se robot pomaknu
a vrati novo polje vjerojatnosti (konoluirano)
'''
def move(p,U):
    l = len(p) #pomocna varijabla
    q = [0] * l #pomocno polje iste duzine
    for i in range(l): #postavimo pojedine celije na ispravne vrijednosti
        q[(i)%l] += p[(i-U) %l] * pExact # += jer svaku celiju mjenjamo do 3 puta
        q[(i-1)%l] += p[(i-U) %l] * pUnder # osim celije u koju ocekujemo da cemo doc podesavamo i celiju prije/poslje
        q[(i+1)%l] += p[(i-U) %l] * pOver # za slucaj da na robot ode predaleko ili stane prerano
    return q

'''
main funkcija
za testiranje
'''
for i in range(3):
    P = sense(P,measure[i])
    print(P)
    P = move(P,motions[i])
    print(P)
'''
kraj main-a
'''
