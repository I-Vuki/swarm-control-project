n = 5
P = [1./n] * n
W = ['g','r','r','g','g']
mesure = ['g', 'r']
pH = 0.6
pM = 0.2
'''
f-ja sense prima polje vjerojatnosti p
i neko znanje Z (informaciju)
te vrati polje novih vrijednosti uz poznat Z
'''
def sense(p,Z):
    q = [0] * len(p)
    for i in range(len(p)):
        if W[i] == Z:
            q[i] = p[i] * pH
        else:
            q[i] = p[i] * pM
    S = sum(q)
    for i in range(len(q)):
        q[i] /= S
    return q
'''
funkcija prima polje vjeroajtnosti p
i broj koraka U za koji se robot pomaknu
a vrati novo polje vjerojatnosti (konoluirano)
'''
def move(p,U):
    l = len(p)
    q = [0] * l
    for i in range(l):
        q[i] = p[(i-U) %l]
'''
for it in mesure:
    P = sense(P,it)
print P
'''

