n = 5
P = [1./n] * n
W = ['g','r','r','g','g']
mesure = ['r', 'r']
motion = [1,1]
pH = 0.6 #koeficijen kad je tocno
pM = 0.2 #koeficijent kad je netocno
pE = 0.8 #koef kad se tocno pomako
pO = 0.1 #koef da je ic predaleko
pU = 0.1 #koef vjerojatnost da ce stat prerano
'''
f-ja sense prima polje vjerojatnosti p
i neko znanje Z (informaciju)
te vrati polje novih vrijednosti uz poznat Z
'''
def sense(p,Z):
    q = [0] * len(p)
    for i in range(len(p)):
        if W[i] == Z:
            q[i] = p[i] * pH
        else:
            q[i] = p[i] * pM
    S = sum(q)
    for i in range(len(q)):
        q[i] /= S
    return q
'''
funkcija prima polje vjeroajtnosti p
i broj koraka U za koji se robot pomaknu
a vrati novo polje vjerojatnosti (konoluirano)
'''
def move(p,U):
    l = len(p)
    q = [0] * l
    for i in range(l):
        q[(i)%l] += p[(i-U) %l] * pE
        q[(i-1)%l] += p[(i-U) %l] * pU
        q[(i+1)%l] += p[(i-U) %l] * pO
    return q

for i in range(2):
    P = sense(P,mesure[i])
    P = move(P,motion[i])

print P

