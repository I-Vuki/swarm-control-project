##############
#KALMAN FILAR#
##############
'''
 funkcija radi update mediana i varijance kalan filtra tako da uzme u obzir predhodnu varijanuc i median te median i varijancu
 mjerenjta, medan se update tako da uzma u obzir i koji je gauss bio sigurniji
'''
def update(u1,o1,u2,o2):
    nu = (o2*u1 + o1*u2)/(o1+o2)
    no = 1./((1./o1)+(1./o2))
    return [nu,no]
'''
Funkcija radi update pomaka tako da uzme gauss pomaka te predhodni gauss. Te je dovoljno da samo zbroji te vdijednosti
'''
def predict(u1,o1,u2,o2):
    nu = u1 + u2
    no = o1 + o2
    return [nu,no]

measurments = [5.,6.,7.,9.,10.]
motion = [1.,1.,2.,1.,1.]
measurment_sig = 4.
motion_sig = 2.
mu = 0.
sig = 10000.
for i in range(len(measurments)):
    [mu, sig] = update(measurments[i], measurment_sig, mu, sig)
    print "update: ", [mu, sig]
    [mu, sig]= predict(motion[i], motion_sig, mu, sig)
    print "predict: ", [mu, sig]
