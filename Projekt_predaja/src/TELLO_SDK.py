import socket
import threading
import time
import traceback
import sys

class Tello_SDK:
    '''
        Python API: kalsa za pristup SDK komandama za ryze tello drone-a
        Pokriva sve osnovne funkcije za komunikaciju s dronom
    '''

    def __init__(self, local_IP='', local_PORT=9000, tello_IP='192.168.10.1', tello_PORT=8889):
        '''
            init funkcija koja postavlja pocetne parametre i otvara UDP socket preme dronu, te postavlja tello u command nacin rada
            Ulazni parametri funkcije:
                local_IP: IP adresa lokalnog racunala, po defaultu je prazna te se nade preko sucelja za mrezu
                local_PORT: PORT# lokalnog racunala, po defaultu je 9000 te tello na njega salje povratne informacije
                tello_IP: IP adresa drona, po defaultu 192.168.10.1 (IP adresa s kojom tello inicijalno i dolazi)
                tello_PORT: PORT# sucelja drona, koji tello slusa za pristigle komande
            Izlazni parametri:
                nema
        '''

        '''
            Postavljanje socketa za komunikaciju izmedu racunala i drona
        '''
        self.tello_addr = (tello_IP,tello_PORT)
        self.host_addr = (local_IP, local_PORT)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(self.host_addr)

        '''
            Interne varijable uz pomoc kojih reguliramo broj pokusaja za dolazak slanja pojedine komande
        '''
        self.report = "error"
        self.tryes = 0

        '''
            Postavljanje tello drona u command mode, kako bi primao komande s racunala a ne vanjsskog kontrolera
        '''
        self.send("command",report=True)
        print("Ulazim u command mode")


    def __del__(self):
        '''
            Funcija se poziva uvijek prilikom unisatavanja objekta, te explicitno zatvara socket prilikom brisanja objekta
            Ulazni parametri:
                nema
            Izlazni parametri:
                nema
        '''
        self.socket.close()


    def send_Tello(self, msg, show=False):
        '''
            Funkcija pokusava poslati poruku (komandu) na tello drona
            Ulazni parametri:
                msg: poruka koju saljemo na tello drona (komanda)
                show: Flag koji, ako je postavljen ispisuje poruku u uspjesnosti slana poruke
            Izlazni parametri:
                nema
        '''
        start_time = time.time()
        try:
            self.socket.sendto(msg.encode('utf-8'), self.tello_addr)
            if(show):
                print("Sending message: " + msg)
        except Exception as e:
            if(show):
                print("Error sending: " + str(e))


    def receive_Tello(self, show=True):
        '''
            Funkija prima poruke s tello drona
            Ulazni parametri:
                show: Flag koji, ako je postavljen ispisuje poruku u uspjesnosti slana poruke
            Izlazni parametri:
                nema
        '''
        try:
            self.report = self.socket.recv(512).decode('utf-8')
            if(show):
                print("Message receiveed: " + self.report)
        except Exeption as e:
            if(show):
                self.report = "error"
                print("Error receiveing: " + str(e))


    def send(self, commands, repeat=1, delay=0, report=True):
        '''
            Napredna funkija za slanje komandi na tello drona, salje jednu ili vise komandi odreden broj puta s postavljenom pauzom izmedu ponavljanja
            Ulazni parametri:
                commands: jedna komanda ili lista komandi koja se salje na tello drona, jedna po jedna
                repeat: broj ponavljana koji se komande izvrsavaju
                deley: timeout vrijeme izmedu komandi u slucaju da se ponavljaju vise od jednom
                report: Zastavica koja odreduje treba li se u konzolu ispisati uspjesnost komande
            Izlazni parametari:
                nema
        '''

        if repeat < 1:
            repeat = 1

        for i in range(repeat):

            if type(commands) is list:
                '''
                    Ako je komanda lista komandi ivrsavaju se jedna po jedna
                '''
                for command in commands:
                    done = False
                    times = 0
                    '''
                        #Komandu pokusa poslati 3 puta, ako se uspjeno posalje ranije prekine pokusaje
                    '''
                    while not done and times < 3:
                        time += 1
                        self.send_Tello(command)
                        self.receive_Tello()
                        if(self.report == "ok"):
                            done = True
                        self.report = "error"
                        if(delay > 0):
                            time.sleep(delay)
            else:
                done = False
                times = 0
                '''
                    #Komandu pokusa poslati 3 puta, ako se uspjeno posalje ranije prekine pokusaje
                '''
                while not done and times < 3:
                    times += 1
                    self.send_Tello(commands)
                    self.receive_Tello()
                    if(self.report == "ok"):
                        done = True
                    self.report = "error"
                    if(delay > 0):
                        time.sleep(delay)

    def takeoff(self):
        '''
            Funkcija za uzlijetanje, ispise poruka nakon uspojesnog uzljetanja
            Ulazni parametri:
                nema
            Izlazni parametri:
                nema
        '''
        #self.send("battery?")
        #if int(self.report) < 9:
        #    print("Niska baterija, nemoguce uzlijetanje, ERROR: EXITING")
        #    sys.exit(0)
        #self.report = "error"
        self.send("takeoff",report=True)
        print("Uzlijecem")


    def land(self):
        '''
            Funkcija za slijetanje, ispise poruka nakon uspojesnog sljetanja
            Ulazni parametri:
                nema
            Izlazni parametri:
                nema
        '''
        self.report = "error"
        self.send("land",report=True)
        print("Slijecem")


    def move_up(self, distance=0):
        '''
            Funkcija za kretanje prema gore,
            Ulazni parametri:
                distance: udaljenesot za koju se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if distance < 20:
            print("Premala distanca")
            return
        elif distance > 500:
            print ("Prevelika dsitanca")
            return
        else:
            self.send("up %d" %(distance))


    def move_down(self, distance=0):
        '''
            Funkcija za kretanje prema dole,
            Ulazni parametri:
                distance: udaljenesot za koju se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if distance < 20:
            print("Premala distanca")
            return
        elif distance > 500:
            print("Prevelika distanca")
            return
        else:
            self.send("down %d" %(distance))


    def rotate_left(self, deg=0):
        '''
            Funkcija za kretanje rotaciju u lijevo (suprotno od smijera kazaljke na satu),
            Ulazni parametri:
                deg: kut za koji se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if deg < 1:
            print("Premalen kut")
            return
        elif deg > 360:
            times = deg/360
            self.send("ccw %d" %(deg%360))
            self.send("ccw %d" %(360), repeat=times)
            return
        else:
            self.send("ccw %d" (deg))


    def rotate_right(self,deg=0):
        '''
            Funkcija za kretanje rotaciju u desno (u smijera kazaljke na satu),
            Ulazni parametri:
                deg: kut za koji se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if deg < 1:
            print("Premalen kut")
            return
        elif deg > 360:
            times = deg/360
            self.send("cw %d" %(deg%360))
            self.send("cw %d" %(360), repeat=times)
            return
        else:
            self.send("cw %d" (deg))


    def move_forward(self, distance=0):
        '''
            Funkcija za kretanje naprijed,
            Ulazni parametri:
                distance: udaljenesot za koju se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if distance < 20:
            print("Premala distanca")
            return
        elif distance > 500:
            print("Prevelika distanca")
            return
        else:
            self.send("forward %d" %(distance))


    def move_back(self, distance=0):
        '''
            Funkcija za kretanje nazad,
            Ulazni parametri:
                distance: udaljenesot za koju se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if distance < 20:
            print("Premala distanca")
            return
        elif distance > 500:
            print("Prevelika distanca")
            return
        else:
            self.send("back %d" %(distance))


    def move_left(self, distance=0):
        '''
            Funkcija za kretanje lijevo,
            Ulazni parametri:
                distance: udaljenesot za koju se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if distance < 20:
            print("Premala distanca")
            return
        elif distance > 500:
            print("Prevelika distanca")
            return
        else:
            self.send("left %d" %(distance))


    def move_right(self, distance=0):
        '''
            Funkcija za kretanje desno,
            Ulazni parametri:
                distance: udaljenesot za koju se tello drone pomakne
            Izlazni parametri:
                nema
        '''
        if distance < 20:
            print("Premala distanca")
            return
        elif distance > 500:
            print("Prevelika distanca")
            return
        else:
            self.send("right %d" %(distance))


    def flip(self, side = ""):
        '''
            Funkcija za okretanje drona,
            Ulazni parametri:
                side: os oko koje se tello drone preokrene
            Izlazni parametri:
                nema
        '''
        if(side == "f"):
            self.send("flip %c" %(side))
            return
        if(side == "b"):
            self.send("flip %c" %(side))
            return
        if(side == "l"):
            self.send("flip %c" %(side))
            return
        if(side == "r"):
            self.send("flip %c" %(side))
            return
        else:
            print("ERROR, nepoznata strana")
            return


    def _emergency(self):
        '''
            Posebna funkcija u slucajnu nuzde zagasi sve motore odjedno, no questions asked. Ne koristiti osim u slucaju stvane nuzde
            Ulazni parametri:
                nema
            Izlazni parametari:
                nema
        '''
        self.send_Tello("emergency")

    def set_speed(self, speed=50):
        '''
            Funkcija za postavljanje brzine,
            Ulazni parametri:
                speed: brzina kojom zelimo da se tello dron krece
            Izlazni parametri:
                nema
        '''
        self.send("speed %d" %(speed))


    def get_speed(self):
        '''
            Funkcija za dobivanje trenutne vrijednosti brizne,
            Ulazni parametri:
                nema
            Izlazni parametri:
                speed: trenutna brizna kojoj tello drone izvrsava svoje komande kretanja
        '''
        self.send_Tello("speed?")
        self.receive_Tello()
        speed = self.report
        return speed


    def get_battery(self):
        '''
            Funkcija za dobivanje trenutnog stanja baterije,
            Ulazni parametri:
                nema
            Izlazni parametri:
                battery: trenutno stanje baterije u postotku
        '''
        self.send_Tello("battery?")
        self.receive_Tello()
        battery = self.report
        return speed


    def get_time(self):
        '''
            Funkcija za dobivanje vremena koje je tello proveo u zraku,
            Ulazni parametri:
                nema
            Izlazni parametri:
                time: vrijeme koje je tello u zraku izrazeno u sekundama
        '''
        self.send_Tello("time?")
        self.receive_Tello()
        time = self.report
        return time


    def get_height(self):
        '''
            Funkcija za dobivanje trenutne visine tello drona,
            Ulazni parametri:
                nema
            Izlazni parametri:
                h: visina na kojoj se tello dron nalazi izrazeno u cm
        '''
        self.send_Tello("height?")
        self.receive_Tello()
        h = self.report
        return time


    def curve(self, x1=50, y1=-50, z1=0, x2=50, y2=50, z2=0, speed=50):
        '''
            Funkcija za odletit krivulju ("Circumcircular Arc") odredenu s tri tocke, ako je samo poznavan odleti slovo s prema naprijed
            1. tocka je trenutna pozicija tello drona (uzima se kao 0,0,0), dok su druge dvije zadane kao parametri funkcije
            Ulazni parametri (vrijednosti x,y,z koordinata racuna se ozbirom na trenutnu poziciju tello drona):
                 x1: x koordinata prva tocke, ako je pozitivna dron ide naprijed, ako je negativan dron ide nazad
                 y1: y koordinata prva tocke, ako je pozitivna dron ide lijevo, ako je negativan dron ide desno
                 z1: z koordinata prva tocke, ako je pozitivna dron ide gore, ako je negativan dron ide dole
                 x2: x koordinata druge tocke, ako je pozitivna dron ide naprijed, ako je negativan dron ide nazad
                 y2: y koordinata druge tocke, ako je pozitivna dron ide lijevo, ako je negativan dron ide desno
                 z2: z koordinata druge tocke, ako je pozitivna dron ide gore, ako je negativan dron ide dole
                speed: brzina kojom se tello drone krece
            Izlazni parametri:
                nema
        '''
        self.send("curve %d %d %d %d %d %d %d" %(x1, y1, z1, x2, y2, z2, speed))


    def move(self, x=50, y=0, z=0, speed=50):
        '''
            Funkcija za odletit ravnu liniju odredenu s ddvije tocke
            1. tocka je trenutna pozicija tello drona (uzima se kao 0,0,0), dok je druga zadana preko parametara funkcije
            Ulazni parametri (vrijednosti x,y,z koordinata racuna se ozbirom na trenutnu poziciju tello drona):
                 x: x koordinata prva tocke, ako je pozitivna dron ide naprijed, ako je negativan dron ide nazad
                 y: y koordinata prva tocke, ako je pozitivna dron ide lijevo, ako je negativan dron ide desno
                 z: z koordinata prva tocke, ako je pozitivna dron ide gore, ako je negativan dron ide dole
                speed: brzina kojom se tello drone krece
            Izlazni parametri:
                nema
        '''
        self.send("go %d %d %d %d" %(x, y, z, speed))


    def circule(self, r = 50, speed = 50):
        '''
            Funkcija za odletit kruznicu, ako je samo pozvana odleti kruznicu radijusa 50cm
            Ulazni parametri:
                r: radijus kuznice koju tello dron iscrta
                speed: brzina kojom se tello drone krece
            Izlazni parametri:
                nema
        '''
        self.send("curve %d %d %d %d %d %d %d" %(-r, -r, 0, -2*r, 0, 0, speed))
        self.send("curve %d %d %d %d %d %d %d" %(r, r, 0, 2*r, 0, 0, speed))


    def rect(self, a=50, speed=50):
        '''
            Funkcija za odletit kvadrat zadane stranice, ako je samo pozvana duljina stranice je 50cm
            Ulazni parametri:
                a: duzljina duzine stranijce kvadrata
                speed: brzina kojom se tello drone krece
            Izlazni parametri:
                nema
        '''
        command1 = "forward %d" %(a)
        command2 = "ccw 90"
        self.send([command1, comand2], repeat=4)


    def bounce(self, height=100, times=5):
        '''
            Funkcija za bounce tello drona
            Ulazni parametri:
                height: visina do koje tello drone ce se dizad tokom bounce-a
                times: broj ponavljanja koji ce se bounce ponovit
            Izlazni parametri:
                nema
        '''
        command1 = "up %d" %(height)
        command2 = "down %d" %(height)
        self.send([command1, comand2], repeat=times)
