import time
import socket
import pygame
import pygame.locals
from math import pi
from TELLO_SDK import Tello_SDK

def bbs_chek(new_pos, size, bbs):
    '''
    Funkcija provjerava jesmo li izasli iz bounding box-a
    za ulazne parametre prima:
        new_pos: uredeni par (x,y) koordinata, pozicija drona na koju se zelimo pomaknuti
        size: uredeni par (x,y), visina i sirina prostorije koju dron vidi
        bbs: bounding box size, uredeni par (x,y), visina i sirina bounding box-a
    za izlaz vraca:
        new_pos: uredeni par(x,y) koordinate, pozicija do koje ce se dron pomaknuti prije sljetanja
        True/Flase: vrojednost koja kaze jesmo li pokusali izaci iz boundig box-a (True, ako jesmo. False, ako nismo)
    '''
    if(new_pos[0] > 0 and new_pos[0] < size[0] and new_pos[1] > 0 and new_pos[1] < bbs):
        new_pos = [new_pos[0], bbs]
        return new_pos, True

    if(new_pos[0] > 0 and new_pos[0] < bbs and new_pos[1] > 0 and new_pos[1] < size[1]):
        new_pos = [bbs, new_pos[1]]
        return new_pos, True

    if(new_pos[0] > 0 and new_pos[0] < size[0] and new_pos[1] > size[1] - bbs and new_pos[1] < size[1]):
        new_pos = [new_pos[0], size[1] - bbs]
        return new_pos, True

    if(new_pos[0] > size[0] - bbs and new_pos[0] < size[0] and new_pos[1] > 0 and new_pos[1] < size[1]):
        new_pos = [size[0] - bbs, new_pos[1]]
        return new_pos, True

    return new_pos, False


'''
    Inicijalirizra pygame sucenje
'''
pygame.init()

'''
    Definira konstante, imena boja
    Boje su uredene troje u RGB zapisu
'''
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GRAY = (220, 220, 220)
BLUE =  (0, 0, 255)
GREEN = (0, 255, 0)
RED =   (255, 0, 0)

'''
    Unos velicine prostorije, Predpostavka da su visina i sirina prostorije iste
    Te postavljanje ostaih velicina potrebne za izvrsavanje
    velicina prostorije, sredina prostorije te velicina bounding box-a
'''
s = input("Unesite velicinu cijele prostorije (cm): ")
size = [s*2, s*2]
mid = [s, s]
bbs = mid[0] - input("slobodni prostor(cm): ")

'''
    Postavljanje osnovnog displaya pygame-a i naslova istog
'''
screen = pygame.display.set_mode(size)
pygame.display.set_caption("tello, flight map")

'''
    Postavljanje pocetnih vrijednosti i globalnih varijabli koje se koriste u programu
    Zastavice
    Pocetne pozicije drona, u katezijevom, dronovom te koordinatnom sustavu ekrana
'''
done = False #Zastavica koja nam odreduje jesmo li gotovo s osnovnim loopom
moved = False #Zastavica koja nam govori jesmo li se pomakli (nema iscrtavanja prije prvog pomaka)
tello = False #Zastavica koja nam govori postoji li pommak u zadnjem koraku
clock = pygame.time.Clock() #Sat kojim se korisitmo za iscrtavanje i event-handeling

new_pos = (0,0) #Uredeni par (x,y) Trenutna pozicija drona (na koju se pomicemo), u katezijevom koordinatnom sustavu
old_pos = (0,0) #Uredeni par (x,y) Pozicija s koje je dron krenuo, u katezijevom koordinatnom sustavu

new_pos_draw = (s, s) #Uredeni par (x,y), Trenutna pozicija drona (na koju se pomicemo), u koordinatnom sustavu ekrana
old_pos_draw = [] #Lista uredenih parova (x,y) svih pozicija na kojima je dron bio, u koordinatnom sustavu ekrana (kako bi se mogla iscrtati njegova putanja)
r = 50
centars = [] #Lista uredenih parova (x,y), centri kruzinca koje je drone napravio, bitno za iscrtavanje

'''
    Inicijalizacija drona te uzljetanje istog
'''
drone = Tello_SDK()
drone.takeoff()

'''
    Osnovna petlja unutar koje se sve odvija
'''
while not done:

    clock.tick(10) #Ogranicavanje resursa kako petja nebi zagusila CPU

    '''
        Projera svih evenata koju su se dogododili, te odradivanje odgovarajuceg zadatka za svaku
        Program se moze zaustavilit pritiskom na 3 nacina:
            X windowa koordinatnog sutava
            Pritiskom na tipku ESC
            Pokusajem izlazka izvan granica bounding box-a

        Dronom se moze uravljat na 2 nacina:
            Uz pomoc misa: 2D po apscisi i ordinati
            Uz pomoc tipki: Za podesavanje visine i posebne funkcijonalnosti (predpostavka je da dron nece izaci iz range-a bounding box-a tokom istih), kao sto su oradivanje kruznice te okreatanje drona

    '''
    for event in pygame.event.get():

        if event.type == pygame.QUIT: #Pritisnut je X widova
            done=True

        if event.type == pygame.MOUSEBUTTONUP: #Korisnik je pritinuo unutar koordinatnog sutava
            old_pos = new_pos
            old_pos_draw.append(new_pos_draw)
            new_pos = pygame.mouse.get_pos()
            new_pos, done = bbs_chek(new_pos, size, bbs)
            new_pos_draw = new_pos
            new_pos = (new_pos[0]-mid[0],(-(new_pos[1]-mid[1])))
            moved = True
            tello = True

        elif event.type == pygame.KEYDOWN: #Pritisnuta je neka tipka

            if event.key == pygame.K_ESCAPE: #Tipka je ESC
                done = True

            if event.key == pygame.K_w: #Tipka je w
                drone.move_up(50)

            if event.key ==  pygame.K_s: #Tipka je s
                drone.move_down(50)

            if event.key == pygame.K_f: #Tipka je f
                drone.flip("f")

            if event.key == pygame.K_b: #Tipka je b
                drone.flip("b")

            if event.key == pygame.K_c: #Tipka je c
                drone.circule()
                centars.append([new_pos_draw[0], new_pos_draw[1]+r])
                moved = True


    '''
        Ciscenje ekrana te postavljanje pozadine u bijelo
        Te postavljanje bounding box-a u sivo
    '''
    screen.fill(WHITE)
    pygame.draw.rect(screen, GRAY, [0, 0, size[0], bbs])
    pygame.draw.rect(screen, GRAY, [0, 0, bbs, size[1]])
    pygame.draw.rect(screen, GRAY, [0, size[1] - bbs, size[0], size[1]])
    pygame.draw.rect(screen, GRAY, [size[0] - bbs, 0, size[0], size[1]])

    '''
        Iscritavanje mreze koordinatnog sutava svakih 50px (50cm stvarnog pomaka)
        Linije grida crne su boje
    '''
    for i in range(0,size[0],50):
        pygame.draw.line(screen, BLACK, [i, 0], [i,size[1]], 1)
    for j in range(0,size[1],50):
        pygame.draw.line(screen, BLACK, [0, j], [size[0],j], 1)

    '''
        Ispis linija za apscisu i ordinatu koordinatnog sustava
        Linije su prave boje
    '''
    pygame.draw.line(screen,BLUE,[mid[0],0], [mid[0], size[1]],2)
    pygame.draw.line(screen,BLUE,[0,mid[1]], [size[0],mid[1]], 2)


    '''
        Iscrtavanje putanje kojojm se dron kretao
        Putanja drona ispisana je crvvenom bojom
    '''
    if(moved):
        if(old_pos_draw):
            for k in range(len(old_pos_draw)-1):
                pygame.draw.line(screen,RED,old_pos_draw[k], old_pos_draw[k+1],2)
            pygame.draw.line(screen,RED,old_pos_draw[-1], new_pos_draw,2)
        if(centars):
            for centar in centars:
                pygame.draw.circle(screen, RED, centar, r, 2)

    '''
        Ako se dron pomaknuo u posljednjem koranu
        Preracunamo koordinate iz katezijevog sutava u sustav drona
        Posaljemo dronu komadnu
    '''

    if(tello):
        tmp_pos = [0,0]
        tmp_pos[0] = (new_pos[0] - old_pos[0])
        tmp_pos[1] = (new_pos[1] - old_pos[1])

        tmp = "go %d %d %d %d" %(tmp_pos[1], -tmp_pos[0], 0, 100)
        print(tmp)
        drone.send(tmp)
        tello = False

    '''
        Zadnja komanda koje se poziva, sve ostale naredbe moraju biti prije -> bez ovoga nemoze se iscrtati nsita
    '''
    pygame.display.flip()


'''
    Sletimo drona nakon izvrsavanja svih komandi
'''
drone.land()
'''
    Zatvorimo GUI za upravljanje dronom
'''
pygame.quit()
