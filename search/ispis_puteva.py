# ----------
# User Instructions:
# 
# Write a function optimum_policy that returns
# a grid which shows the optimum policy for robot
# motion. This means there should be an optimum
# direction associated with each navigable cell from
# which the goal can be reached.
# 
# Unnavigable cells as well as cells from which 
# the goal cannot be reached should have a string 
# containing a single space (' '), as shown in the 
# previous video. The goal cell should have '*'.
# ----------

grid = [[0, 1, 0, 0, 0, 0], 
        [0, 1, 1, 0, 1, 0], 
        [0, 0, 0, 0, 1, 0], 
        [0, 1, 1, 1, 1, 0], 
        [0, 1, 0, 1, 1, 0]]
init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1 # the cost associated with moving from a cell to an adjacent one

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

def optimum_policy(grid,goal,cost):
    value = [[99 for col in range(len(grid[0]))] for row in range(len(grid))]
    bio = [[0 for col in range(len(grid[0]))] for row in range(len(grid))]
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
    x = goal[0]
    y = goal[1]
    value[x][y] = 0
    bio[x][y] = 1
    cek = []
    cek.append([x,y])
    while len(cek):
        next = cek.pop()
        x = next[0]
        y = next[1]
        bio[x][y] = 1
        mi = 99
        if(grid[x][y] == 0):
            for it in delta:
                x2 = x + it[0]
                y2 = y + it[1]
                if((x2 >= 0 and x2 < (len(grid))) and (y2 >= 0 and y2 < (len(grid[0])))):
                    if (bio[x2][y2]):
                            mi = min(mi, value[x2][y2])
                    else:
                        cek.append([x2,y2])
            if([x,y] != goal ):
                value[x][y] = mi + cost
    
    for x in range(len(value)):
        for y in range(len(value[x])):
            mi = 99
            for i in range(len(delta)):
                x2 = x + delta[i][0]
                y2 = y + delta[i][1]
                if((x2 >= 0 and x2 < (len(grid))) and (y2 >= 0 and y2 < (len(grid[0]))) and grid[x][y] == 0):
                    nmi = min(mi, value[x2][y2])
                    if(nmi != mi):
                        ni = i
                        mi = nmi
            if(value[x][y] != 99):
                policy[x][y] = delta_name[ni]
    
    policy[goal[0]][goal[1]] = '*'
    return policy
    
x = optimum_policy(grid,goal,cost)
for it in x:
    print it
